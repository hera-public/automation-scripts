import fs from 'fs'
import path from 'path'

import YAML from 'yaml'

const CONFIG_DIR = './config/'


var files = fs.readdirSync(CONFIG_DIR)
if (files && files.length > 0){
    var file = files[0];
    if (path.extname(file) == ".yml"){
        console.log("Fetching "+file);
        const yaml_file = fs.readFileSync(CONFIG_DIR+file, 'utf8')
        var json_conf = YAML.parse(yaml_file)

        console.log(json_conf)
        // TODO. validate configuration
    }
}



